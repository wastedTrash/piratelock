<GameFile>
  <PropertyGroup Name="MainScene" Type="Scene" ID="a2ee0952-26b5-49ae-8bf9-4f1d6279b798" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" ctype="GameNodeObjectData">
        <Size X="750.0000" Y="1334.0000" />
        <Children>
          <AbstractNodeData Name="background" ActionTag="1553503633" Tag="191" IconVisible="False" HorizontalEdge="BothEdge" VerticalEdge="BothEdge" LeftMargin="-169.1250" RightMargin="-160.8750" TopMargin="-295.4011" BottomMargin="-290.5988" ctype="SpriteObjectData">
            <Size X="1080.0000" Y="1920.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="370.8750" Y="669.4012" />
            <Scale ScaleX="0.6933" ScaleY="0.7003" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4945" Y="0.5018" />
            <PreSize X="1.4400" Y="1.4393" />
            <FileData Type="Normal" Path="background.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="lock" ActionTag="1948732442" Tag="190" IconVisible="False" HorizontalEdge="BothEdge" VerticalEdge="BothEdge" LeftMargin="-162.5250" RightMargin="-167.4750" TopMargin="-162.9292" BottomMargin="-46.0708" ctype="SpriteObjectData">
            <Size X="1080.0000" Y="1543.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="377.4750" Y="725.4292" />
            <Scale ScaleX="0.6081" ScaleY="0.6081" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5033" Y="0.5438" />
            <PreSize X="1.4400" Y="1.1567" />
            <FileData Type="Normal" Path="lock.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="pathLayer" ActionTag="311435735" Tag="137" RotationSkewY="-0.0035" IconVisible="True" HorizontalEdge="BothEdge" VerticalEdge="BothEdge" LeftMargin="377.4751" RightMargin="372.5249" TopMargin="746.5759" BottomMargin="587.4241" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="0.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="377.4751" Y="587.4241" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5033" Y="0.4403" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="exitButton" ActionTag="1677470818" Tag="224" IconVisible="False" LeftMargin="434.8600" RightMargin="86.1400" TopMargin="41.2272" BottomMargin="1214.7728" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="199" Scale9Height="56" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="229.0000" Y="78.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="549.3600" Y="1253.7728" />
            <Scale ScaleX="1.5242" ScaleY="1.5242" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7325" Y="0.9399" />
            <PreSize X="0.3053" Y="0.0585" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="buttonPressed.png" Plist="" />
            <NormalFileData Type="Normal" Path="button.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="retryButton" ActionTag="1927897734" Tag="58" IconVisible="False" LeftMargin="435.9336" RightMargin="85.0664" TopMargin="1196.8912" BottomMargin="59.1088" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="199" Scale9Height="56" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="229.0000" Y="78.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="550.4336" Y="98.1088" />
            <Scale ScaleX="1.5242" ScaleY="1.5242" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7339" Y="0.0735" />
            <PreSize X="0.3053" Y="0.0585" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="buttonRetryPressed.png" Plist="" />
            <NormalFileData Type="Normal" Path="buttonRetry.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="nextButton" ActionTag="-302100226" Tag="59" IconVisible="False" LeftMargin="65.0379" RightMargin="455.9621" TopMargin="1194.9886" BottomMargin="61.0114" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="199" Scale9Height="56" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="229.0000" Y="78.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="179.5379" Y="100.0114" />
            <Scale ScaleX="1.5242" ScaleY="1.5242" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2394" Y="0.0750" />
            <PreSize X="0.3053" Y="0.0585" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="buttonNextPressed.png" Plist="" />
            <NormalFileData Type="Normal" Path="buttonNext.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>