#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "Level.h"
#include <vector>
#include "ui\UIWidget.h"
#include "base\CCTouch.h"
#include "base\CCEvent.h"

class HelloWorld : public cocos2d::Layer
{
	public:
		// there's no 'id' in cpp, so we recommend returning the class instance pointer
		static cocos2d::Scene* createScene();

		// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
		virtual bool init();

		//������� ������� �� ������ "�����"
		void exitPressed(Ref *sender, cocos2d::ui::Widget::TouchEventType type);

		//������� ������� �� ������ "���������"
		void nextPressed(Ref *sender, cocos2d::ui::Widget::TouchEventType type);

		//������� � ������� �����
		void lockTouched(cocos2d::Touch* touch, cocos2d::Event* event);
		
		//������� � ������� �����
		bool lockBeganTouch(cocos2d::Touch* touch, cocos2d::Event* event);

		//������� ������������� ������
		void initLockLevel();

		//������������ �������
		void drawLockLevel();

		// implement the "static create()" method manually
		CREATE_FUNC(HelloWorld);
	private:
		//���� �������� �����, � ������� ���������� ��� ���� �������
		Node *rootNode, *lockPath;
		//��������� �������, ����������� ��������� �������� ������ � �����������������
		//(��, �������, � �� �������. ������ ������ ��� ��� ������� ������ ����������� ������� �������� ������)
		std::vector< std::vector<int> > curLevel, level1, level2, level3;
		//������ ������ � ��� ���������� �����
		int levelSize,levelNum;
		std::vector<Level> levels;
};

#endif // __HELLOWORLD_SCENE_H__
