#include "Level.h"


Level::Level(){
	levelSize = 0;
	currentPosition = Point(-1, -1);
}

Level::Level(int  *arrayLevel, int arraySize){
	levelSize = arraySize;
	for (int i(0); i < levelSize; ++i){
		originLevel.push_back(std::vector<int>(levelSize, 1));
		for (int j(0); j < levelSize; ++j){
			originLevel[i][j] = arrayLevel[i*levelSize + j];
			if (originLevel[i][j] == 2)
				currentPosition = Point(i, j);
		}
	}
	activeLevel = loseLevel = Design(originLevel);
}

void Level::retry(){//reset level to its origin state
	activeLevel = loseLevel = Design(originLevel);
	bool positionReseted = false;
	for (int i(0); i < levelSize && !positionReseted; ++i){
		for (int j(0); j < levelSize && !positionReseted; ++j)
			if (activeLevel[i][j] == 2){
				currentPosition = Point(i, j);
				positionReseted = !positionReseted;
				break;
			}


	}
}

void Level::loseRetry(){//if user lost last game, level rotates in 90deg
	Design temp(loseLevel);
	for (int i(0); i < levelSize; ++i)
		for (int j(0); j < levelSize; ++j){
			loseLevel[j][levelSize - i - 1] = temp[i][j];
			if (temp[i][j] == 2)
				currentPosition = Point(j, levelSize - i - 1);
		}
	activeLevel = Design(loseLevel);
}

bool Level::isGameOver(){ //checks are some moves possible 
						 //true - user's got no possible moves
						//false - user can move somewhere
	int i(currentPosition.x), j(currentPosition.y);
	bool noMoveH = false, noMoveV = false;
	
		if ((j + 1 == levelSize) && ((activeLevel[i][j - 1] == 0) || (activeLevel[i][j - 1] == 3)))
			noMoveH = true;
		else {
			if ((j - 1 < 0) && ((activeLevel[i][j + 1] == 0) || (activeLevel[i][j + 1] == 3)))
				noMoveH = true;
			else if ((j + 1 < levelSize) && (j - 1 >= 0))
					if (((activeLevel[i][j + 1] == 0) || (activeLevel[i][j + 1] == 3)) && ((activeLevel[i][j - 1] == 0) || (activeLevel[i][j - 1] == 3)))
						noMoveH = true;
		}

		if ((i + 1 == levelSize) && ((activeLevel[i - 1][j] == 0) || (activeLevel[i - 1][j] == 3)))
			noMoveV = true;
		else {
			if ((i - 1 < 0) && ((activeLevel[i + 1][j] == 0) || (activeLevel[i + 1][j] == 3)))
				noMoveV = true;
			else if ((i + 1 < levelSize) && (i - 1 >= 0))
					if (((activeLevel[i + 1][j] == 0) || (activeLevel[i + 1][j] == 3)) && ((activeLevel[i - 1][j] == 0) || (activeLevel[i - 1][j] == 3)))
						noMoveV = true;
		}


	if (noMoveH && noMoveV) 
		return true;
	else return false;
}

bool Level::isWin(){ //checks are some moves possible 
	//true - user's got no possible moves
	//false - user can move somewhere
	for (int i(0); i < levelSize; ++i)
		for (int j(0); j < levelSize; ++j)
			if (activeLevel[i][j] == 1)
				return false;
	return true;
}

Level::Point Level::getCurrentPosition(){ //returns 
	return currentPosition;
}

bool Level::move(int dx, int dy){
	int i(currentPosition.x), j(currentPosition.y);
	bool moved = false;
	if (dx > 0){ //going right
		while ((j + 1 < levelSize) && (activeLevel[i][j + 1] != 3) && (activeLevel[i][j + 1] != 0)){
			activeLevel[i][j] = 3; //set path trace
			j++;
			activeLevel[i][j] = 2; //set next cursor's position
		}
		moved = !moved;
	}
	if (dx < 0){ //going left
		while ((j - 1 >= 0) && (activeLevel[i][j - 1] != 3) && (activeLevel[i][j - 1] != 0)){
			activeLevel[i][j] = 3; //set path trace
			j--;
			activeLevel[i][j] = 2; //set next cursor's position
		}
		moved = !moved;
	}
	if (dy < 0){ //going up
		while ((i + 1 < levelSize) && (activeLevel[i+1][j] != 3) && (activeLevel[i + 1][j] != 0)){
			activeLevel[i][j] = 3; //set path trace
			i++;
			activeLevel[i][j] = 2; //set next cursor's position
		}
		moved = !moved;
	}
	if (dy > 0){ //going down
		while ((i - 1 >= 0) && (activeLevel[i-1][j] != 3) && (activeLevel[i - 1][j] != 0)){
			activeLevel[i][j] = 3; //set path trace
			i--;
			activeLevel[i][j] = 2; //set next cursor's position
		}
		moved = !moved;
	}

	if (moved){
		currentPosition = Point(i,j);
		return true;
	}
	else return false;
}

void Level::setBlockSize(float size){
	blockSize = size;
}

float Level::getBlockSize(){
	return blockSize;
}

int Level::lenght(){
	return levelSize;
}

Level::~Level(){
	
}

