#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include <string>
#include <array>
#include "ui\UIButton.h"

USING_NS_CC;

using namespace cocostudio::timeline;



Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
	//��������� ������ �������
	levelNum = 0;	 //����� ������ - 1
	
	levelSize = 5; //������ ������ - 5. 
	//����� ��� �� ����� ��� ���� �� ����� ������� ��������������� ������	

	int level1[5][5] = { { 1, 1, 1, 1, 1 }, { 1, 2, 1, 1, 1 }, { 1, 1, 1, 1, 1 }, { 1, 1, 1, 1, 1 }, { 1, 1, 1, 1, 1 } };//������ ������� � �����
	int level2[5][5] = { { 0, 0, 2, 1, 1 }, { 1, 1, 1, 1, 1 }, { 1, 1, 1, 1, 0 }, { 1, 1, 0, 1, 1 }, { 1, 1, 1, 1, 1 } };
	int level3[6][6] = { { 1, 1, 1, 1, 1, 1 }, { 1, 1, 1, 1, 0, 1 }, { 0, 1, 1, 2, 1, 1 }, { 1, 1, 1, 1, 1, 0 }, { 1, 0, 1, 1, 1, 1 }, { 1, 1, 1, 1, 1, 1 } };

	levels.push_back(Level((int *)level1, levelSize));
	levels.push_back(Level((int *)level2, levelSize));
	levelSize = 6;
	levels.push_back(Level((int *)level3, levelSize));
	checked = true;

    if ( !Layer::init() )
    {
        return false;
    }
    

	//��������� ���� �������� �����
    rootNode = CSLoader::createNode("MainScene.csb");
	//� ���� �������� ����� ���� ���� "pathLayer", ���������� ��������� ��� ��������� ������
	lockPath = rootNode->getChildByName("pathLayer");
	lockPath->setPosition(Vec2(377.48f, 574.22f)); //������������� ��������� ����
	lockPath->setContentSize(Size(450.0f, 470.0f)); //������������� ������ ����
	//������� ���� ���� � ������������ �� ��� ����
	initLockLevel();

	//��������� �������� �� ������� ������ "�����"
	cocos2d::ui::Button *exitButton = (cocos2d::ui::Button *)rootNode->getChildByName("exitButton");
	exitButton->addTouchEventListener(CC_CALLBACK_2(HelloWorld::exitPressed, this) );

	//��������� �������� �� ������� ������ "���������"
	cocos2d::ui::Button *nextButton = (cocos2d::ui::Button *)rootNode->getChildByName("nextButton");
	nextButton->addTouchEventListener(CC_CALLBACK_2(HelloWorld::nextPressed, this));


	//��������� �������� �� ������� ������ "������"
	cocos2d::ui::Button *retryButton = (cocos2d::ui::Button *)rootNode->getChildByName("retryButton");
	retryButton->addTouchEventListener(CC_CALLBACK_2(HelloWorld::retryPressed, this));

	//��������� ���� �������� ����� � ���� ����� �����
    addChild(rootNode);

	//�������� ������ �����
	this->scheduleUpdate();

    return true;
}



void HelloWorld::initLockLevel(){//������������ ������� �����
	auto lockTouchListener = EventListenerTouchOneByOne::create();
	lockTouchListener->setSwallowTouches(true);
	lockTouchListener->onTouchBegan = CC_CALLBACK_2(HelloWorld::lockBeganTouch, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(lockTouchListener, this);
	
	Size visibleSize = lockPath->getContentSize(); //�������� ���� � �������� ����
	for (int i(0); i < levels.size(); ++i)
		levels[i].setBlockSize(visibleSize.width / levels[i].lenght());

	drawLockLevel();
}

void HelloWorld::drawLockLevel(){
	//������� �� ���� ��, ��� ���� ������
	lockPath->removeAllChildren();

	float rect_width = levels[levelNum].getBlockSize();
	
	//����� ����� ���� �� ������ ������

	//��������� ������
	for (int i(0); i<levels[levelNum].lenght(); ++i){
		for (int j(0); j<levels[levelNum].lenght(); ++j){
			if (levels[levelNum].activeLevel[i][j] != 0){
				auto sprite = Sprite::create("Blue.png");

				sprite->setTextureRect(Rect(0, 0, rect_width, rect_width));
				sprite->setAnchorPoint(Vec2(0.5, 0.5));
				sprite->setPosition(Vec2((j + 0.5)*rect_width - lockPath->getContentSize().width / 2,
					-(i + 0.6)*rect_width + lockPath->getContentSize().height / 2));

				lockPath->addChild(sprite);
			}
			if (levels[levelNum].activeLevel[i][j] == 2){
				auto sprite = Sprite::create("yellow.png");
				sprite->setName("yellowBlock");
				sprite->setZOrder(50);
				sprite->setTextureRect(Rect(0, 0, rect_width, rect_width));
				sprite->setAnchorPoint(Vec2(0.5, 0.5));
				sprite->setPosition(Vec2((j + 0.5)*rect_width - lockPath->getContentSize().width / 2,
					-(i + 0.6)*rect_width + lockPath->getContentSize().height / 2));

				lockPath->addChild(sprite);
			}
		}

	}
}

//������� ������� �� ������ "�����"
void HelloWorld::exitPressed(Ref *sender, cocos2d::ui::Widget::TouchEventType type){
	if (type == cocos2d::ui::Widget::TouchEventType::ENDED)
		exit(0);
}


//������� ������� �� ������ "���������"
void HelloWorld::nextPressed(Ref *sender, cocos2d::ui::Widget::TouchEventType type){
	if (type == cocos2d::ui::Widget::TouchEventType::ENDED){
		levels[levelNum].retry();
		if (levelNum == 2)
			levelNum = 0;
		else ++levelNum;
		//������������ �������
		drawLockLevel();
	}
}

//������� ������� �� ������ "������"
void HelloWorld::retryPressed(Ref *sender, cocos2d::ui::Widget::TouchEventType type){
	if (type == cocos2d::ui::Widget::TouchEventType::ENDED){
		levels[levelNum].retry();
		//������������ �������
		drawLockLevel();
	}
}

bool HelloWorld::lockBeganTouch(cocos2d::Touch* touch, cocos2d::Event* event){
	Point horizontalLimits = Point(lockPath->getPosition().x - lockPath->getContentSize().width/2, lockPath->getPosition().x + lockPath->getContentSize().width/2);
	Point verticalLimits = Point(lockPath->getPosition().y - lockPath->getContentSize().height/2, lockPath->getPosition().y + lockPath->getContentSize().height/2);
	Point currentPositionHor = Point(lockPath->getChildByName("yellowBlock")->getPosition().x - levels[levelNum].getBlockSize()/2, lockPath->getChildByName("yellowBlock")->getPosition().x + levels[levelNum].getBlockSize()/2);
	Point currentPositionVer = Point(lockPath->getChildByName("yellowBlock")->getPosition().y - levels[levelNum].getBlockSize()/2, lockPath->getChildByName("yellowBlock")->getPosition().y + levels[levelNum].getBlockSize()/2);
	Point toched = touch->getLocation();
	if ((toched.x > (horizontalLimits.x))
		&&
		(toched.x < (horizontalLimits.y))
		&&
		(toched.y > (verticalLimits.x))
		&&
		(toched.y < (verticalLimits.y))
		){//���� ������ � ������� ������ ����� (� ������� �����������)
			Point prevPos = Point(levels[levelNum].getCurrentPosition().x, levels[levelNum].getCurrentPosition().y);
			toched = Point(lockPath->convertToNodeSpace(touch->getLocation()));
			if ((toched.y > currentPositionVer.x)
				&&
				(toched.y < currentPositionVer.y)
				){ //���� ������� �� ����� ������ � ����� ������ �� �����������
					if (toched.x < currentPositionHor.x){
						//���� ������� ����� ������ �����, ���������� ���� �����
						if (levels[levelNum].move(-1, 0))//���� ���� ������������, ������������
							moveBlocks(prevPos);
					}
					else if (toched.x > currentPositionHor.y)
						//���� ������� ������ ������ �����, ���������� ���� �����
						if (levels[levelNum].move(1, 0))//���� ���� ������������, ������������
							moveBlocks(prevPos);
				}
			else 
					if ((toched.x > currentPositionHor.x)
						&&
						(toched.x < currentPositionHor.y)
					){ //���� ������� �� ����� ������ � ����� ������ �� ���������
						if (toched.y < currentPositionVer.x){
							//���� ������� ���� ������ �����, ���������� ���� ����
							if (levels[levelNum].move(0, -1))//���� ���� ������������, ������������
								moveBlocks(prevPos);
						}
						else if (toched.y > currentPositionVer.y)
							//���� ������� ���� ������ �����, ���������� ���� �����
							if (levels[levelNum].move(0, 1))//���� ���� ������������, ������������
								moveBlocks(prevPos);
					}
		return true;
	}
	else return false;

}

void HelloWorld::moveBlocks(cocos2d::Point prevPos){
	//disable touches until animation is over
	Director::getInstance()->getEventDispatcher()->pauseEventListenersForTarget(this);

	float duration = 0.35;
	auto moveBy = MoveBy::create(duration, Vec2((levels[levelNum].getCurrentPosition().y - prevPos.y)*levels[levelNum].getBlockSize(), (prevPos.x - levels[levelNum].getCurrentPosition().x)*levels[levelNum].getBlockSize()));
	
	lockPath->getChildByName("yellowBlock")->runAction(moveBy);
	int traceLenght = 0;
	int positive = 0, vertical = 0, horizontal = 0;
	
	if (levels[levelNum].getCurrentPosition().x - prevPos.x != 0){
		traceLenght = prevPos.x - levels[levelNum].getCurrentPosition().x;
		vertical = 1;
	}
	else {
		traceLenght = levels[levelNum].getCurrentPosition().y - prevPos.y;
		horizontal = 1;
	}
	Point prevState = lockPath->getChildByName("yellowBlock")->getPosition();
	
	if (traceLenght > 0)
		positive = 1;
	else positive = -1;
	
	for (int i(0); i <= abs(traceLenght); ++i){
		auto pathTrace = Sprite::create("red.png");
		auto scaleAnimation = ScaleTo::create(duration*2.5 / (abs(traceLenght) - i), 1);

		std::string name = std::to_string(i) + ":" + std::to_string(prevState.x) + ";" + std::to_string(prevState.y);
		pathTrace->setName(name);
		pathTrace->setScale(0);
		pathTrace->setTextureRect(Rect(0, 0, levels[levelNum].getBlockSize(), levels[levelNum].getBlockSize()));
		pathTrace->setAnchorPoint(Vec2(0.5, 0.5));
		pathTrace->setPosition(prevState + Vec2(positive*horizontal*(i)*levels[levelNum].getBlockSize(), positive*vertical*(i)*levels[levelNum].getBlockSize()));
		lockPath->addChild(pathTrace);
		pathTrace->runAction(scaleAnimation);
		
	}
	checked = false;
	//isGameOver();

}

void HelloWorld::update(float delta){
	if ((checked == false ) && (lockPath->getChildByName("yellowBlock")->numberOfRunningActions() == 0)){
		isGameOver();
		checked = true;
		Director::getInstance()->getEventDispatcher()->resumeEventListenersForTarget(this);
	}
}

void HelloWorld::isGameOver(){
	if (levels[levelNum].isWin())
		cocos2d::MessageBox("Congratulations! You won!", "WIN");
	else if (levels[levelNum].isGameOver()){
		cocos2d::MessageBox("You lost! Try again", "LOSE");
		levels[levelNum].loseRetry();
		drawLockLevel();
	}
}