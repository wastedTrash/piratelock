#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "Level.h"
#include <vector>
#include <math.h>
#include "ui\UIWidget.h"
#include "base\CCTouch.h"
#include "base\CCEvent.h"

class HelloWorld : public cocos2d::Layer
{
	public:
		// there's no 'id' in cpp, so we recommend returning the class instance pointer
		static cocos2d::Scene* createScene();

		// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
		virtual bool init();

		//������� ������� �� ������ "�����"
		void exitPressed(Ref *sender, cocos2d::ui::Widget::TouchEventType type);

		//������� ������� �� ������ "���������"
		void nextPressed(Ref *sender, cocos2d::ui::Widget::TouchEventType type);
		
		//������� ������� �� ������ "������"
		void retryPressed(Ref *sender, cocos2d::ui::Widget::TouchEventType type);
		
		//������� � ������� �����
		bool lockBeganTouch(cocos2d::Touch* touch, cocos2d::Event* event);

		//������� ������������� ������
		void initLockLevel();

		void isGameOver();

		//������������ �������
		void drawLockLevel();

		void moveBlocks(cocos2d::Point prevPos);

		void update(float) override;

		// implement the "static create()" method manually
		CREATE_FUNC(HelloWorld);
	private:
		//���� �������� �����, � ������� ���������� ��� ���� �������
		Node *rootNode, *lockPath;
		//��������� �������, ����������� ��������� �������� ������ � �����������������
		//(��, �������, � �� �������. ������ ������ ��� ��� ������� ������ ����������� ������� �������� ������)
		//������ ������ � ��� ���������� �����
		bool checked;
		int levelSize,levelNum;
		std::vector<Level> levels;
};

#endif // __HELLOWORLD_SCENE_H__
