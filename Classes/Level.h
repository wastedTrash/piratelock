#pragma once 
#ifndef LEVEL_H
#define LEVEL_H

#include <vector>

class Level
{
	private:
		typedef std::vector< std::vector< int > > Design;
	public:
		struct Point{
			int x;
			int y;
			Point(){
				x = 0; y =0;
			}
			Point(int inX, int inY){
				x = inX; y = inY;
			}
		};
		Design activeLevel;
		Level();
		Level(int *arrayLevel, int arraySize);
		void retry();
		void loseRetry();
		bool isGameOver();
		bool isWin();
		Level::Point getCurrentPosition();
		bool move(int dx, int dy);
		void setBlockSize(float size);
		float getBlockSize();
		int lenght();
		~Level();
	private:
		Design originLevel, loseLevel;
		Point currentPosition;
		int levelSize;
		float blockSize;
};

#endif // LEVEL_H