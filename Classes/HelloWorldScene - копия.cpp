#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include <string>
#include <array>
#include "ui\UIButton.h"

USING_NS_CC;

using namespace cocostudio::timeline;



Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
	cocos2d::MessageBox("Hello", "!!Welcome!!");
	//��������� ������ �������
	levelNum = 1;	 //����� ������ - 1
	
	levelSize = 5; //������ ������ - 5. 
	//����� ��� �� ����� ��� ���� �� ����� ������� ��������������� ������

	//�������. 
	//����� ������������ ������� �������.x
	std::vector<int> tempVec(5,1);
	level1.push_back(tempVec);
	tempVec[1] = 2;
	level1.push_back(tempVec);
	tempVec[1] = 1;
	level1.push_back(tempVec);
	level1.push_back(tempVec);
	level1.push_back(tempVec);
	/*���, ��� ���������� � ������� level1
	1, 1, 1, 1, 1 
	1, 2, 1, 1, 1
	1, 1, 1, 1, 1
	1, 1, 1, 1, 1
	1, 1, 1, 1, 1
	*/

	tempVec[0] = tempVec[1] = 0; tempVec[2] = 2;
	level2.push_back(tempVec); 
	tempVec[0] = tempVec[1] = tempVec[2] = 1;
	level2.push_back(tempVec);
	tempVec[4] = 0;
	level2.push_back(tempVec);
	tempVec[4] = 1; tempVec[2] = 0;
	level2.push_back(tempVec);
	tempVec[2] = 1;
	level2.push_back(tempVec);
	/*���, ��� ���������� � ������� level2
	0, 0, 2, 1, 1
	1, 1, 1, 1, 1
	1, 1, 1, 1, 0
	1, 1, 0, 1, 1
	1, 1, 1, 1, 1
	*/

	tempVec.push_back(1);
	level3.push_back(tempVec);
	tempVec[4] = 0;
	level3.push_back(tempVec);
	tempVec[0] = 0; tempVec[3] = 2; tempVec[4] = 1;
	level3.push_back(tempVec);
	tempVec[0] = 1; tempVec[3] = 1; tempVec[5] = 0;
	level3.push_back(tempVec);
	tempVec[1] = 0; tempVec[5] = 1;
	level3.push_back(tempVec);
	tempVec[1] = 1;
	level3.push_back(tempVec);
	tempVec.pop_back();
	/*���, ��� ���������� � ������� level3
	1, 1, 1, 1, 1, 1
	1, 1, 1, 1, 0, 1
	0, 1, 1, 2, 1, 1
	1, 1, 1, 1, 1, 0
	1, 0, 1, 1, 1, 1
	1, 1, 1, 1, 1, 1
	*/
	

	//��������� ������� �������� ������
	for (int i(0); i < 5; ++i){
		for (int j(0); j < 5; ++j){
			tempVec[j] = level1[i][j];
		}

		curLevel.push_back(tempVec);
	}
	

	int levelEx[5][5] = { { 1, 1, 1, 1, 1 }, { 1, 2, 1, 1, 1 }, { 1, 1, 1, 1, 1 }, { 1, 1, 1, 1, 1 }, { 1, 1, 1, 1, 1 } };//������ ������� � �����

	levels.push_back(Level((int *)levelEx, levelSize));
	
	levels[0].loseRetry();

    if ( !Layer::init() )
    {
        return false;
    }
    

	//��������� ���� �������� �����
    rootNode = CSLoader::createNode("MainScene.csb");
	//� ���� �������� ����� ���� ���� "pathLayer", ���������� ��������� ��� ��������� ������
	lockPath = rootNode->getChildByName("pathLayer");
	lockPath->setPosition(Vec2(377.48f, 574.22f)); //������������� ��������� ����
	lockPath->setContentSize(Size(450.0f, 470.0f)); //������������� ������ ����
	//������� ���� ���� � ������������ �� ��� ����
	initLockLevel();

	//��������� �������� �� ������� ������ "�����"
	cocos2d::ui::Button *exitButton = (cocos2d::ui::Button *)rootNode->getChildByName("exitButton");
	exitButton->addTouchEventListener(CC_CALLBACK_2(HelloWorld::exitPressed, this) );

	//��������� �������� �� ������� ������ "���������"
	cocos2d::ui::Button *nextButton = (cocos2d::ui::Button *)rootNode->getChildByName("nextButton");
	nextButton->addTouchEventListener(CC_CALLBACK_2(HelloWorld::nextPressed, this));

	//��������� ���� �������� ����� � ���� ����� �����
    addChild(rootNode);

    return true;
}



void HelloWorld::initLockLevel(){//������������ ������� �����
		auto lockTouchListener = EventListenerTouchOneByOne::create();
		lockTouchListener->setSwallowTouches(true);
		lockTouchListener->onTouchEnded = CC_CALLBACK_2(HelloWorld::lockTouched, this);
		lockTouchListener->onTouchBegan = CC_CALLBACK_2(HelloWorld::lockBeganTouch, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(lockTouchListener, this);
	drawLockLevel();
}

void HelloWorld::drawLockLevel(){
	//������� �� ���� ��, ��� ���� ������
	lockPath->removeAllChildren();

	Size visibleSize = lockPath->getContentSize(); //�������� ���� � �������� ����
	int rect_width = visibleSize.width / levelSize; //������ ������ ��������
	//����� ����� ���� �� ������ ������

	//��������� ������
	for (int i(0); i<levelSize; ++i){
		for (int j(0); j<levelSize; ++j){
			if (curLevel[i][j] != 0){
				auto sprite = Sprite::create("Blue.png");

				sprite->setTextureRect(Rect(0, 0, rect_width, rect_width));
				sprite->setAnchorPoint(Vec2(0.5, 0.5));
				sprite->setPosition(Vec2((j + 0.5)*rect_width - lockPath->getContentSize().width / 2,
					-(i + 0.6)*rect_width + lockPath->getContentSize().height / 2));

				lockPath->addChild(sprite);
			}
			if (curLevel[i][j] == 2){
				auto sprite = Sprite::create("yellow.png");
				sprite->setZOrder(50);
				sprite->setTextureRect(Rect(0, 0, rect_width, rect_width));
				sprite->setAnchorPoint(Vec2(0.5, 0.5));
				sprite->setPosition(Vec2((j + 0.5)*rect_width - lockPath->getContentSize().width / 2,
					-(i + 0.6)*rect_width + lockPath->getContentSize().height / 2));

				lockPath->addChild(sprite);
			}

		}

	}
}

//������� ������� �� ������ "�����"
void HelloWorld::exitPressed(Ref *sender, cocos2d::ui::Widget::TouchEventType type){
	if (type == cocos2d::ui::Widget::TouchEventType::ENDED)
		exit(0);
}


//������� ������� �� ������ "���������"
void HelloWorld::nextPressed(Ref *sender, cocos2d::ui::Widget::TouchEventType type){
	if (type == cocos2d::ui::Widget::TouchEventType::ENDED){
		curLevel.clear();//������� ������� �������� ������
		switch (levelNum) //������� ����� ������� ������� � ������ �� ����� ����������
			{
			case 1:{
				levelNum++; //��������� �� ������� 2
				levelSize = 5; //������ ������ ��� - 5
				std::vector<int> tempVec(5, 1);

				//������������ ������� ������ ��� � ������� �������� ������
				for (int i(0); i < 5; ++i){
					for (int j(0); j < 5; ++j){
						tempVec[j] = level2[i][j]; 
					}

					curLevel.push_back(tempVec);
				}
				break;
			}
			case 2:{
				levelNum++;//��������� �� ������� 3
				levelSize = 6;//������ ������ ��� - 6
				std::vector<int> tempVec(6, 1);

				//������������ ������� ������ ��� � ������� �������� ������
				for (int i(0); i < 6; ++i){
					for (int j(0); j < 6; ++j){
						tempVec[j] = level3[i][j];
					}

					curLevel.push_back(tempVec);
				}
				
				break;
			}
			case 3:{
				levelNum = 1;//��������� �� ������� 1
				levelSize = 5;//������ ������ ���� - 5
				std::vector<int> tempVec(5, 1);

				//������������ ������� ������ ���� � ������� �������� ������
				for (int i(0); i < 5; ++i){
					for (int j(0); j < 5; ++j){
						tempVec[j] = level1[i][j];
					}

					curLevel.push_back(tempVec);
				}
				break;
			}
		}
		//������������ �������
		drawLockLevel();
	}
}

void HelloWorld::lockTouched(cocos2d::Touch* touch, cocos2d::Event* event){
	cocos2d::MessageBox("Wow! Touched", "!!TOUCH!!");
}

bool HelloWorld::lockBeganTouch(cocos2d::Touch* touch, cocos2d::Event* event){
	cocos2d::MessageBox("Wow! Touched", "!!TOUCH!!");
	return true;
}